# Paulaner® Spezi flag
![Spezi flag](./spezi_flag.svg)

This probably originated on fedi at some point. I found some other flag online but it did not match the colors on the Paulaner Spezi website ([Wayback Machine image](http://web.archive.org/web/0/https://www.paulaner.de/assets/images/modules/product_intro/pl_spezi_welle_sidebild_new.jpg)). The aspect ratio is 5:3.

## Colors
From top to bottom:
* `#f9b700`
* `#ec6400`
* `#e6001c`
* `#e30083`
* `#4b1e7d`
