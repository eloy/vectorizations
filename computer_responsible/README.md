# IBM slide about computers and management
![slide](./computer_responsible.svg)

The remainder of the presentation was lost in a flood according to the person who originally posted it on Twitter. The orginal font is most likely (capitalized) Helvetica, since that's what IBM used for decades before developing their Plex font. Article [here](https://web.archive.org/web/20171116090158/https://qz.com/1124664/ibm-plex-with-its-first-ever-custom-corporate-font-ibm-is-freeing-itself-from-the-tyranny-of-helvetica/).
