Original design by Deutsche Telekom

Fonts used:
* Garamond #3 Bold Italic Old Style Figures by Linotype AG for 'nicht' and 'jetzt'.
* Helvetica Bold for remainder 

I am not completely sure if these are the fonts used in the original, but they were the closest match I could find
