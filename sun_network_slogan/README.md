# Sun Microsystems: The Network is the Computer
![The Network is the Computer](/sun_network_slogan.svg)

Flag design inspired on the following marketing sheet by Sun: https://web.archive.org/web/0/https://www.filibeto.org/sun/lib/hardware/sunray/ds/fullbro1000.pdf

Sun hired the German desing firm LucasFonts for their corporate identity. More info here: https://www.lucasfonts.com/fonts/sun/info
