![I have been VAX-inated](/vax_button_pin.svg)
# VAX-inated 
Vector recreation of the original pin button by Digital Equipment Corporation with Inkscape. It is not color accurate since this I only had a single low quality image as a source. The original does seem to something like beige/ivory, but I like the full white more. The background blue is a random guess as well.

# Properties
* Font used is ITC Avant Garde
* The Digital logo is an earlier version, from 1987
* Letter spacing is -3.0

# Copyright
Both the VAX and Digital logo are in the public domain because they are not above the required originality treshold and I think that neither the button is copyrightable. I release any additional contributions under the CC0 license.

# Links
* [VAX-inated button pin](https://www.computerhistory.org/collections/catalog/102682110), Computer History Museum
* Batchelder, Nad. [Ancient history: the Digital logo](https://nedbatchelder.com/blog/200712/ancient_history_the_digital_logo.html). 16 Dec 2007
