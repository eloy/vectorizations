Vectorized poster for cable radio in Switzerland. For more info, see https://en.wikipedia.org/wiki/Cable_radio#Switzerland

Source: https://poster-gallery.com/images/products/250/poster_250041_z.jpg

The used typeface is Futura, see https://en.wikipedia.org/wiki/Futura_(typeface)

Original author: Pubaco Bienne
