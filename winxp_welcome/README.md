# Windows XP welcome screen
![Windows XP welcome](/Windows_XP_welcome.svg)

[Picture as a flag at 38c3](https://hsnl.social/@eloy/113721474245549472)

* Font is Arial Bold Italic
* Colors are found using the color picker in Inkscape, the three gradient are made by hand but are not exactly the same
* Sizes of different blocks are not very precise either to make them better suitable for a flag

The small bars consists of a gradient of three colors

Top:
* left: in between blue shades
* middle: white-ish
* right: same blue as middle of image

Botton:
* left: blue that is even darker than the big bar
* middle: orange
* right: same a the big bar on the bottom

Video of Windows XP booting: https://youtu.be/Ulmw-pwjmU4?feature=shared&t=10
